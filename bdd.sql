
CREATE TABLE recipe (
    id int NOT NULL PRIMARY KEY,
    name varchar(64) NOT NULL,
    category varchar(64) NOT NULL,
    picture varchar(64) NOT NULL,
    score int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE recipe (
    id int NOT NULL PRIMARY KEY,
    name varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE ingredient MODIFY id int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE recipe MODIFY id int(11) NOT NULL AUTO_INCREMENT;



INSERT INTO recipe VALUES ('sucre');

INSERT INTO recipe VALUES (1, 'lasagne', 'plat', 'aucune', 5);

INSERT INTO recipe VALUES (2, 'gateau_chocolat', 'dessert', 'aucune', 5);

INSERT INTO recipe VALUES (3, 'spaghetti', 'plat', 'aucune', 3);

INSERT INTO recipe VALUES (4, 'salade_nicoise', 'entree', 'aucune', 4);

INSERT INTO recipe VALUES (5, 'crepe', 'dessert', 'aucune', 5);

INSERT INTO recipe VALUES (6, 'gratin', 'plat', 'aucune', 3);


