<?php

class Database{

    private $host = "localhost";
    private $db_name = "ApiFondDePlacard";
    private $username = "root";
    private $password = "";
    public  $connexion;

    public function getConnection(){
        $this->connexion = null;

        try{
            $this->connexion = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->connexion->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Echec de la connexion : " . $exception->getMessage();
        }

        return $this->connexion;
    }
}
