<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: PUT");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if($_SERVER['REQUEST_METHOD'] == 'PUT'){

    include_once '../config/database.php';
    include_once '../models/recipe.php';

    $database = new Database();
    $db = $database->getConnection();

    $recipe = new Recipes($db);

    $donnees = json_decode(file_get_contents("php://input"));

    if(!empty($donnees->id) && !empty($donnees->name) && !empty($donnees->category) && !empty($donnees->picture) && !empty($donnees->score)){

        $recipe->id = $donnees->id;
        $recipe->name = $donnees->name;
        $recipe->category = $donnees->category;
        $recipe->picture = $donnees->picture;
        $recipe->score = $donnees->score;

        if($recipe->modifier()){

            http_response_code(200);
            echo json_encode(["message" => "La modification a été effectuée"]);
        }else{

            http_response_code(503);
            echo json_encode(["message" => "La modification n'a pas été effectuée"]);
        }
    }
}else{

    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
}
