<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if($_SERVER['REQUEST_METHOD'] == 'GET'){

    include_once '../config/database.php';
    include_once '../models/recipe.php.php';

    $database = new Database();
    $db = $database->getConnection();

    $recipe = new Recipes($db);

    $lecture = $produit->lire();

    if($lecture->rowCount() > 0){
        $tableauRecipes = [];
        $tableauRecipes['recipes'] = [];

        while($row = $lecture->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $precipe = [
                "id" => $id,
                "name" => $name,
                "category" => $category,
                "picture" => $picture,
                "score" => $score
            ];

            $tableauRecipes['recipes'][] = $recette;
        }

        http_response_code(200);

        echo json_encode($tableauRecipes);
    }

}else{

    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
}
