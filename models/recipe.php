<?php

class Recipe{

    private $connexion;
    private $table = "recipe";

    public $id;
    public $name;
    public $category;
    public $picture;
    public $score;

    /**
     * @param $db
     */
    public function __construct($db){
      $this->connexion = $db;
    }

    /**
     * @return void
     */
    public function lire(){

      $sql = "SELECT r.id, r.name, r.category, r.picture, r.score FROM " . $this->table .

      $query = $this->connexion->prepare($sql);

      $query->execute();

      return $query;
    }

    /**
     * @return void
     */
    public function creer(){

      $sql = "INSERT INTO " . $this->table . " SET name=:name, category=:category, picture=:picture, score=:score";

      $query = $this->connexion->prepare($sql);

      $this->name=htmlspecialchars(strip_tags($this->name));
      $this->category=htmlspecialchars(strip_tags($this->category));
      $this->picture=htmlspecialchars(strip_tags($this->picture));
      $this->score=htmlspecialchars(strip_tags($this->score));

      $query->bindParam(":name", $this->name);
      $query->bindParam(":category", $this->category);
      $query->bindParam(":picture", $this->picture);
      $query->bindParam(":score", $this->score);

        if($query->execute()){
            return true;
        }
        else {
        return false;
        }

    /**
     * @return void
     */
    function lireUn(){
      $sql = "SELECT r.id, r.name, r.category, r.picture, r.score FROM " .$this->table.

      $query = $this->connexion->prepare( $sql );

      $query->bindParam(1, $this->id);

      $query->execute();

      $row = $query->fetch(PDO::FETCH_ASSOC);

      $this->name = $row['name'];
      $this->category = $row['category'];
      $this->picture = $row['picture'];
      $this->score = $row['score'];
    }

    /**
     * @return void
     */
    function supprimer(){

      $sql = "DELETE FROM " . $this->table . " WHERE id = ?";

      $query = $this->connexion->prepare( $sql );

      $this->id=htmlspecialchars(strip_tags($this->id));

      $query->bindParam(1, $this->id);

      if($query->execute()){
          return true;
      }
      else {
      return false;
      }
    }

    /**
     * @return void
     */
     function modifier(){

      $sql = "UPDATE " . $this->table . " SET name=:name, category=:category, picture=:picture, score=:score WHERE id = :id";

      $query = $this->connexion->prepare($sql);

      $this->name=htmlspecialchars(strip_tags($this->name));
      $this->category=htmlspecialchars(strip_tags($this->category));
      $this->picture=htmlspecialchars(strip_tags($this->picture));
      $this->score=htmlspecialchars(strip_tags($this->score));

      $query->bindParam(':name', $this->name);
      $query->bindParam(':category', $this->category);
      $query->bindParam(':picture', $this->picture);
      $query->bindParam(':score', $this->score);
      $query->bindParam(':id', $this->id);

        if($query->execute()){
            return true;
        }
        else {
        return false;
        }
      }

}
