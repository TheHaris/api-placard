<?php

class ingredient{

  private $connexion;
  private $table = "ingredient";

  public $id;
  public $name;

  public function __construct($db)
  {
    $this->connexion = $db;
  }

  public function lireIngredient()
  {
    $sql = "SELECT i.id, i.name FROM" . $this->table .

    $query = $this->connexion->prepare($sql);

    $query->execute();

    return $query;

  }

  public function creerIngredient()
  {
    $sql = "INSERT INTO " . $this->table . "SET name=:name";

    $query = $this->connexion->prepare($sql);

    $this->name=htmlspecialchars(strip_tags($this->name));

    $query->bindParam(":name", $this->name);

    if($query->execute()){

      return true;
    }
    else {

      return false;
    }
  }

  public function lireUnIngredient()
  {
    $sql = "SELECT i.id i.name FROM " .$this->table.

    $query = $this->connexion->prepare( $sql );

    $query->bindParam(1, $this->id);

    $query->execute();

    $row = $query->fetch(PDO::FETCH_ASSOC);

    $this->name = $row['name'];

  }

  public function modifierIngredient()
  {

    $sql = "UPDATE " . $this->table . " SET name=:name WHERE id = :id";

    $query = $this->connexion->prepare($sql);

    $this->name=htmlspecialchars(strip_tags($this->name));

    $query->bindParam(':name', $this->name);

     if($query->execute()){
      return true;
     }
     else {
      return false;
     }
  }

  public function supprimerIngredient()
  {

    $sql = "DELETE FROM " . $this->table . " WHERE id = ?";

    $query = $this->connexion->prepare( $sql );

    $this->id=htmlspecialchars(strip_tags($this->id));

    $query->bindParam(1, $this->id);

    if($query->execute()){
        return true;
    }
    else {
    return false;
    }

  }

}
